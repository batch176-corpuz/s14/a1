let firstName = "Nat";
let lastName = "Corpuz";
let age = "26";
let hobbies = ["drums", "video games", "coding"];
let workAddress = {
	city: "Pasig",
	region: "NCR",
};

function printUserInfo(fName, lName, ageNum) {
	console.log(`Hello! I am ${fName} ${lName} and I am ${ageNum} years old.`);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log("Work address:");
	console.log(workAddress);
}

printUserInfo(firstName, lastName, age);

function returnFunction(randomVal) {
	return randomVal;
}

let isDeveloper = returnFunction(true);

console.log(`Is ${firstName} ${lastName} a Developer? ${isDeveloper}`);
